#include <iostream>
#include <tuple>
#include <fstream>
#include "CSVParser.h"
#include "PrintTuple.h"

using namespace std;

int main() {
    ifstream s("testin.csv");
    CSVParser<int, string, string>parser (s, 0, '\n', ',', '\"');

    for (auto i = parser.begin(); i != parser.end() ; ++i) {
        cout << *i << endl;
    }

//    for (const tuple<int, string, string>& cortege : parser) {
//        cout << cortege << endl;
//    }
}
