#ifndef TASK4_PRINTTUPLE_H
#define TASK4_PRINTTUPLE_H

#include <ostream>
#include <tuple>

template<std::size_t> struct int_{};

template<typename CharT, typename Traits, typename TupleType, size_t Pos>
std::basic_ostream<CharT, Traits> &
printTuple(std::basic_ostream<CharT, Traits> &basicOstream, const TupleType &tuple, int_<Pos>) {
    basicOstream << std::get<std::tuple_size<TupleType>::value - Pos>(tuple) << ", ";
    return printTuple(basicOstream, tuple, int_<Pos - 1>());
}

template<typename CharT, typename Traits, typename TupleType>
std::basic_ostream<CharT, Traits> &
printTuple(std::basic_ostream<CharT, Traits> &basicOstream, const TupleType &t, int_<1>) {
    return basicOstream << std::get<std::tuple_size<TupleType>::value - 1>(t);
}

template<typename CharT, typename Traits, typename ... Types>
std::basic_ostream<CharT, Traits> &operator<<(std::basic_ostream<CharT, Traits> &out, const std::tuple<Types...> &t) {
    out << '(';
    printTuple(out, t, int_<sizeof...(Types)>());
    return out << ')';
}

#endif
