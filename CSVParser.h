#ifndef TO5_CSVPARSER_H
#define TO5_CSVPARSER_H
#include <utility>
#include <vector>
#include <string>
#include <exception>
#include <sstream>
#include <iterator>
enum class ErrorType{
    EmptyLine = 0, UnexpectedValue = 1, ParseError = 2
};
class ParseException: public std::exception {
public:
    ParseException(ErrorType errorType, int lineNumber, int columnNumber,std::string line = "", std::string value = ""){
        this->line = std::move(line);
        this->errorType = errorType;
        this->value = std::move(value);
        this->lineNumber = lineNumber;
        this->columnNumber = columnNumber;
    }
    std::string what() {
        switch (errorType) {
            case ErrorType::EmptyLine:
                return ("Line number " + std::to_string(this->lineNumber) + " is empty");
            case ErrorType::UnexpectedValue:
                return ("Unexpected Value: " + this->value + ", column number is " + std::to_string(this->columnNumber) +
                ", line number is " + std::to_string(this->lineNumber) + ", line: " + this->line);
            case ErrorType::ParseError:
                return ("Unable to parse value: " + this->value + ", column number is " + std::to_string(this->columnNumber) +
                ", line number is " + std::to_string(this->lineNumber) + ", line: " + this->line);
        }
    }

private:
    ErrorType errorType;
    int lineNumber;
    int columnNumber;
    std::string line;
    std::string value;
};

template<typename ... Args> class CSVReader {
public:
    explicit CSVReader(char lineDelimiter = '\n',
                       char valueDelimiter = ',',
                       char fieldDelimiter = '\"') :
            lineDelimiter(lineDelimiter),
            valueDelimiter(valueDelimiter),
            fieldDelimiter(fieldDelimiter) {}

    void readNextTuple(std::istream &istream, std::tuple<Args...> &tuple) {
        try {
            readLine(istream, tuple, std::index_sequence_for<Args...>());
        }
        catch (ParseException &ex) {
            std::cout << ex.what() << std::endl;
            return;
        }
    }
    static bool canRead(std::istream &istream) {
        return istream.good() && istream.peek() != EOF;
    }

private:
    std::string nextLine;
    const char lineDelimiter;
    const char valueDelimiter;
    const char fieldDelimiter;
    std::vector<std::string> lineFields; // поля
    std::vector<size_t> delimitersPos;   // позиции разделителей
    size_t curFieldIndex = 0;
    size_t cntOfReadLines = 0;
    size_t curValuePos = 0;

    enum class CSVState {
        UnescapedField,
        EscapedField,
        EscapedEscape,
    };

    void SplitIntoFields() {
        CSVState state = CSVState::UnescapedField;
        lineFields.clear();
        lineFields.emplace_back("");
        delimitersPos.clear();
        size_t indexCurField = 0;
        size_t curPos = 0;
        for (char c : nextLine) {
            switch (state) {
                case CSVState::UnescapedField:
                    if (c == valueDelimiter) {
                        lineFields.emplace_back("");
                        delimitersPos.push_back(curPos);
                        indexCurField++; // следующее поле
                    } else if (c == fieldDelimiter) {
                        state = CSVState::EscapedField;
                    } else {
                        lineFields[indexCurField].push_back(c);
                    }
                    break;
                case CSVState::EscapedField:
                    if (c == fieldDelimiter) {
                        state = CSVState::EscapedEscape;
                    } else {
                        lineFields[indexCurField].push_back(c);
                    }
                    break;
                case CSVState::EscapedEscape:
                    if (c == valueDelimiter) {
                        lineFields.emplace_back("");
                        delimitersPos.push_back(curPos);
                        indexCurField++;
                        state = CSVState::UnescapedField;
                    } else if (c == fieldDelimiter) {
                        lineFields[indexCurField].push_back(fieldDelimiter);
                        state = CSVState::EscapedField;
                    } else {
                        //lineFields[indexCurField].push_back(c);
                        state = CSVState::UnescapedField;
                    }
                    break;
            }
            curPos++;
        }
    }

    template<size_t ... Idx>
    void readLine(std::istream &i, std::tuple<Args...> &tuple, std::index_sequence<Idx...>) {
        std::getline(i, nextLine, lineDelimiter);
        cntOfReadLines++;
        curValuePos = 0;
        curFieldIndex = 0;
        if (nextLine.empty()) {
            throw ParseException(ErrorType::EmptyLine, cntOfReadLines, 0);
        }
        SplitIntoFields(); // разбить на поля

        (..., (writeValue(std::get<Idx>(tuple)))); // из полей в кортеж

        if (curFieldIndex != lineFields.size()) {
            throw ParseException(ErrorType::UnexpectedValue, cntOfReadLines, curValuePos + 1,
                                 nextLine,lineFields[curFieldIndex]);
        }
    }

    template<typename T>
    void writeValue(T &t) {
        std::string curField = lineFields[curFieldIndex];
        std::stringstream convert(curField);
        if ((convert >> t).fail() || !(convert >> std::ws).eof()) {
            throw ParseException(ErrorType::ParseError, cntOfReadLines, curValuePos, nextLine, curField);
        }
        curValuePos = delimitersPos[curFieldIndex] + 1;
        curFieldIndex++;
    }
};

template<typename ... Types> class CSVIterator {
private:
    typedef std::tuple<Types...> TupleType;
    bool canRead = false;
    std::istream &istream;
    std::tuple<Types...> nextTuple;
    CSVReader<Types...> csvReader;
public:
    explicit CSVIterator(std::istream &basicIstream, char lineDelimiter = '\n', char valueDelimiter = ',',
                         char fieldDelimiter = '\"') :
            istream(basicIstream),
            canRead(basicIstream.good()),
            csvReader(lineDelimiter, valueDelimiter, fieldDelimiter) { ++(*this); }

    explicit CSVIterator(std::istream &s, bool isEnd) : istream(s), canRead(!isEnd), csvReader() {}

    CSVIterator &operator++() { // префикс
        canRead = csvReader.canRead(istream);
        if (canRead) {
            csvReader.readNextTuple(istream, nextTuple);
        }
        return *this;
    }

    CSVIterator operator++(int) { // постфикс
        CSVIterator t(*this);
        ++(*this);
        return t;
    }

    const TupleType &operator*() const {
        return nextTuple;
    }

    const TupleType *operator->() const {
        return &nextTuple;
    }

    bool operator==(const CSVIterator &other) const {
        return this == &other || canRead == other.canRead;
    }

    bool operator!=(const CSVIterator &other) const {
        return !((*this) == other);
    }
};

template<typename ... Args> class CSVParser {
private:
    std::istream &stream_;
    const char lineDelimiter;
    const char valueDelimiter;
    const char fieldDelimiter;
    int skipLinesCount;

public:
    explicit CSVParser(std::istream &stream,
                       int skipLinesCount = 0,
                       char lineDelimiter = '\n',
                       char valueDelimiter = ',',
                       char fieldDelimiter = '\"') :
            stream_(stream),
            lineDelimiter(lineDelimiter),
            valueDelimiter(valueDelimiter),
            fieldDelimiter(fieldDelimiter),
            skipLinesCount(skipLinesCount) {}

    CSVIterator<Args...> begin() const {
        CSVIterator<Args...> iterator(stream_, lineDelimiter, valueDelimiter, fieldDelimiter);
        for(int i = 0; i < skipLinesCount; ++i){
            iterator++;
        }
        return iterator;
    }

    CSVIterator<Args...> end() const {
        CSVIterator<Args...> iterator(stream_, true);
        return iterator;
    }
};

#endif
